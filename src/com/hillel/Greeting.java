package com.hillel;

public class Greeting {
    public static void main(String[] args) {
        System.out.println("5 + 1 = " + (5 + 1));
        prettyMethod();
    }

    private static void prettyMethod() {
        System.out.println("Hello, World");
        System.out.println("1 + 1 = " + (1 + 1));
    }
}
